<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="keywords" content="@yield('keywords','Marijan Gudelj is completing this test')">
  
  <meta name="author" content="Marijan Gudelj">

  <title>Butterfly effected test</title>

  <!-- Bootstrap core CSS -->
<link href="{{asset('css/app.css')}}" rel="stylesheet">

</head>

<body>

    <!-- Page Content -->
    <div class="container modified-container">
      @yield('content')
    </div><!-- /.container -->

  <!-- JavaScript -->
  <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

</body>

</html>