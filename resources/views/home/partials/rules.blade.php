<div class="row">
        <div class="col-5">
            <fieldset class="form-border" >
                <legend>Rule:</legend>
                <div class="form-group row">
                    <label for="rule_name" class="col-sm-2 col-form-label">ID</label><!-- /.col-sm-2 col-form-label -->
                    <div class="col-sm-10">
                        <input type="text" name="rule_name" id="rule_name" 
                        disabled="disabled" readonly="true"
                        class="form-control" required placeholder="ID"
                    @if(count($rules)) value="{{$rules->last()->id}}" @else value="1" @endif >
                    </div><!-- /.col-sm-10 -->
                </div><!-- /.form-group row -->
                
                <div class="form-group row">
                    <label for="rule_name" class="col-sm-2 col-form-label">Name</label><!-- /.col-sm-2 col-form-label -->
                    <div class="col-sm-10">
                        <input type="text" name="rule_name" id="rule_name" 
                        class="form-control" required placeholder="Name">
                    </div><!-- /.col-sm-10 -->
                </div><!-- /.form-group row -->

                <div class="form-group row">
                    <label for="rule_description" class="col-sm-2 col-form-label">Name</label><!-- /.col-sm-2 col-form-label -->
                    <div class="col-sm-10">
                       <textarea name="rule_description" id="rule_description" cols="30" rows="4" 
                       class="form-control" placeholder="Rule description"></textarea><!-- /#.form-control -->
                    </div><!-- /.col-sm-10 -->
                </div><!-- /.form-group row -->
            </fieldset>
            
        </div><!-- /.col-5 -->
        <div class="col-7">
            <h1 class="text-primary">Rules</h1><!-- /.text-primary -->
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        @if(count($rules))
                            @foreach($rules as $rule)
                                <td>{{$rule->id}}</td>
                                <td>{{$rule->name}}</td>
                                <td>{{$rule->description}}</td>
                                <td>
                                    <button class="btn btn-success">@lang('rules.edit')</button><!-- /.btn-success -->
                                    <button class="btn btn-danger">@lang('rules.delete')</button><!-- /.btn btn-danger -->
                                </td>
                            @endforeach
                        @else
                            <td colspan="4" class="text-center">@lang('rules.no-rules-listed')</td>
                        @endif
                    </tr>
                    
                </tbody>
            </table><!-- /.table -->    
        </div><!-- /.col-7 -->    
    </div><!-- /.row -->