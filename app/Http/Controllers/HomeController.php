<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $rules = \App\Rule::all();
        return view('home.index', compact('rules'));
    }
}
