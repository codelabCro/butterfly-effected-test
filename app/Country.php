<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['name'];

    /**
     * relations
     */

    #get types relation
    public function types(){
        return $this->hasMany(Type::class,'country_id');
    }
    
    #get categories relation
    public function categories(){
        return $this->hasManyThrough(Category::class,Type::class);
    }
 
}
