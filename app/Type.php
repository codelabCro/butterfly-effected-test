<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $fillable = ['country_id','name'];

    /**
     * relations
     */

    #get types relation
    public function country(){
        return $this->belongsTo(Conutry::class,'country_id');
    }

    public function categories(){
        return $this->hasMany(Category::class,'type_id');
    }
}
